{ pkgs, ... }:

let
  extensions = with pkgs.gnomeExtensions; [
    alphabetical-app-grid
    appindicator
    blur-my-shell
    caffeine
    dash-to-panel
    gsconnect
    hide-universal-access
    notification-banner-reloaded
    tiling-assistant
    vitals
  ];
in
{
  programs.gnome-shell = {
    enable = true;
    extensions = map (e: { package = e; }) extensions;
  };

  dconf = {
    enable = true;

    settings = {
      # Theme and interface
      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        gtk-theme = "adw-gtk3-dark";
        icon-theme = "Adwaita";
        cursor-theme = "volantes_light_cursors";
        show-battery-percentage = true;
      };

      # Tiling with screen edges
      "org/gnome/mutter".edge-tiling = true;

      # Touchpad setings
      "org/gnome/desktop/peripherals/touchpad".natural-scroll = false;

      "org/gnome/desktop/wm/preferences".button-layout = "appmenu:minimize,close";

      # Shell settings (enable user extensions)
      "org/gnome/shell" = {
        disable-user-extensions = false;
        enabled-extensions = map (e: e.extensionUuid) extensions;
      };

      # Extensions

      "org/gnome/shell/extensions/blur-my-shell/panel".blur = false;

      "org/gnome/shell/extensions/vitals" = {
        fixed-widths = false;
        hot-sensors = [ "_memory_usage_" "_processor_usage_" "__temperature_avg__" "_processor_frequency_" ];
        position-in-panel = 0;
      };

      "org/gnome/shell/extensions/notification-banner-reloaded" = {
        anchor-horizontal = 1;
        animation-direction = 1;
        always-minimized = true;
      };
    };
  };

  home.packages = with pkgs; [
    adw-gtk3
    volantes-cursors
  ];
}
