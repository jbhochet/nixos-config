{ pkgs, ... }:

{
  programs.chromium = {
    enable = true;
    package = pkgs.brave;
    dictionaries = with pkgs.hunspellDictsChromium; [
      fr_FR
      en_US
    ];
    extensions = [
      { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # ublock-origin
      { id = "oldceeleldhonbafppcapldpdifcinji"; } # language-tool
      { id = "dlnejlppicbjfcfcedcflplfjajinajd"; } # bonjourr
      { id = "ghmbeldphafepmbegfdlkpapadhbakde"; } # proton-pass
      { id = "bkdgflcldnnnapblkhphbgpggdiikppg"; } # duckduckgo
    ];
  };
}
