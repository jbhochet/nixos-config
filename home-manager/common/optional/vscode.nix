{ pkgs, ... }:

{
  programs.vscode = {
    enable = true;

    enableUpdateCheck = false;
    enableExtensionUpdateCheck = false;
    mutableExtensionsDir = false;

    userSettings = {
      "window.titleBarStyle" = "custom";

      "editor.rulers" = [ 80 120 ];
      "editor.minimap.enabled" = false;
      "editor.fontFamily" = "'FiraCode Nerd Font Propo', 'Droid Sans Mono', 'monospace', monospace";

      "workbench.colorTheme" = "Catppuccin Mocha";
      "workbench.iconTheme" = "catppuccin-mocha";

      "telemetry.telemetryLevel" = "off";

      "extensions.ignoreRecommendations" = true;
      "extensions.autoUpdate" = false;

      "nix.enableLanguageServer" = true;
      "nix.serverPath" = "nil";
      "nix.serverSettings.nil.formatting.command" = [ "nixpkgs-fmt" ];

      "vscord.behaviour.suppressNotifications" = true;
    };

    extensions = with pkgs.vscode-extensions; [
      # Theme
      catppuccin.catppuccin-vsc
      catppuccin.catppuccin-vsc-icons

      # Discord RPC
      leonardssh.vscord

      # Handle .editorconfig file
      editorconfig.editorconfig

      # Git history
      donjayamanne.githistory

      # Todo
      gruntfuggly.todo-tree

      # Hex support
      ms-vscode.hexeditor

      # Formatter
      esbenp.prettier-vscode

      # Nix
      mkhl.direnv
      jnoortheen.nix-ide

      # Bash
      mads-hartmann.bash-ide-vscode

      # Python
      ms-python.python
      ms-python.vscode-pylance

      # C/C++
      ms-vscode.cpptools
      twxs.cmake
      ms-vscode.cmake-tools
      ms-vscode.makefile-tools

      # Java
      vscjava.vscode-maven
      vscjava.vscode-java-test
      vscjava.vscode-java-dependency
      vscjava.vscode-java-debug
      vscjava.vscode-gradle
      redhat.java

      # Vue.js
      vue.volar

      # XML
      redhat.vscode-xml

      # Golang
      golang.go

      # PlantUML
      jebbs.plantuml

      # Latex
      james-yu.latex-workshop

      # Rust
      rust-lang.rust-analyzer
    ];
  };

  home.packages = with pkgs; [
    nil
    nixpkgs-fmt

    gdb # C/C++ debug
  ];
}
