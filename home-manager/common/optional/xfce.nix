{ pkgs, ... }:

rec {
  # Theme
  gtk = {
    enable = true;
    theme = {
      name = "Flat-Remix-GTK-Blue-Dark-Solid";
      package = pkgs.flat-remix-gtk;
    };
    cursorTheme = {
      name = "volantes_light_cursors";
      package = pkgs.volantes-cursors;
    };
    iconTheme = {
      name = "Flat-Remix-Blue-Dark";
      package = pkgs.flat-remix-icon-theme;
    };
  };

  # Gestures
  xdg.configFile."touchegg/touchegg.conf".text = ''
    <touchégg>
      <settings></settings>

      <application name="All">
        <gesture type="SWIPE" fingers="3" direction="LEFT">
          <action type="CHANGE_DESKTOP">
            <direction>next</direction>
            <cyclic>true</cyclic>
            <animate>true</animate>
            <animationPosition>right</animationPosition>
          </action>
        </gesture>

        <gesture type="SWIPE" fingers="3" direction="RIGHT">
          <action type="CHANGE_DESKTOP">
            <direction>previous</direction>
            <cyclic>true</cyclic>
            <animate>true</animate>
            <animationPosition>left</animationPosition>
          </action>
        </gesture>

      </application>
    </touchégg>
  '';

  # XFCE config
  xfconf.settings = {
    xsettings = {
      "Gtk/CursorThemeName" = gtk.cursorTheme.name;
      "Net/ThemeName" = gtk.theme.name;
      "Net/IconThemeName" = gtk.iconTheme.name;
    };
    xfce4-power-manager = {
      "xfce4-power-manager/lid-action-on-battery" = 1;
      "xfce4-power-manager/lid-action-on-ac" = 1;
    };
    xfwm4 = {
      "general/theme" = "Flat-Remix-Dark-XFWM";
      "general/vblank_mode" = "glx";
      # Windows tiling
      "general/tile_on_move" = true;
      "/general/wrap_windows" = false;
    };
  };
}
