{ pkgs, ... }:

{
  programs.helix = {
    enable = true;

    settings = {
      theme = "catppuccin_mocha";
      editor = {
        cursorline = true;
        cursorcolumn = true;
        rulers = [ 80 120 ];
        bufferline = "always";
        color-modes = true;
      };
      /* wait for the next release ;)
      editor.inline-diagnostics = {
        cursor-line = "hint";
        other-line = "error";
      };
      */
    };

    ignores = [ "!.gitignore" "!.gitattributes" ];

    extraPackages = with pkgs; [
      # Nix
      nil
    ];
  };
}
