{ ... }:

{
  imports = [
    ./direnv.nix
    ./git.nix
    ./helix.nix
    ./zsh.nix
  ];
}
