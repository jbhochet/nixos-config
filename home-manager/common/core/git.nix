{ ... }:

{
  programs.git = {
    enable = true;
    userName = "Jb Hochet";
    userEmail = "jean-baptiste.hochet@proton.me";
  };
}
