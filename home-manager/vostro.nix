{ pkgs, ... }:

{
  imports = [
    ./common/core

    ./common/optional/brave.nix
    ./common/optional/eclipse.nix
    ./common/optional/gnome.nix
    ./common/optional/libreoffice.nix
    ./common/optional/vscode.nix
  ];

  home.packages = with pkgs; [
    obsidian # markdown notes
    protonvpn-gui # private VPN
    keepassxc # passwords manager
    discord # social network
    rnote # stylus notes
  ];

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "24.05";
}
