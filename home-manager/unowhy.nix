{ ... }:

{
  imports = [
    ./common/core

    ./common/optional/brave.nix
    ./common/optional/xfce.nix
    ./common/optional/libreoffice.nix
  ];

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "24.05";
}
