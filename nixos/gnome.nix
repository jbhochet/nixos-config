{ pkgs, ... }:

{
  # Enable Gnome desktop and GDM
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };

  # Set the default session
  services.displayManager.defaultSession = "gnome";

  # Allow ports for GSConnect
  networking.firewall = {
    allowedUDPPortRanges = [
      { from = 1716; to = 1764; }
    ];
    allowedTCPPortRanges = [
      { from = 1716; to = 1764; }
    ];
  };

  # Gnome packages
  environment.systemPackages = with pkgs; [
    gnome-tweaks
    terminator
  ];

  environment.gnome.excludePackages = (with pkgs; [
    gnome-tour
    epiphany
    geary
    gnome-contacts
    gnome-maps
  ]);
}
