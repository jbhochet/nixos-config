{ pkgs, ... }:

{
  services.xserver = {
    enable = true;

    # Enable lightdm
    displayManager.lightdm.enable = true;

    # Enable XFCE
    desktopManager.xfce.enable = true;
  };

  # This is bad
  services.libinput.touchpad.tappingDragLock = false;

  # Manage secrets
  services.gnome.gnome-keyring.enable = true;

  # Gestures
  services.touchegg.enable = true;

  # Packages
  environment.systemPackages = with pkgs; [
    # XFCE plugins
    xfce.xfce4-pulseaudio-plugin
    xfce.xfce4-whiskermenu-plugin
    xfce.xfce4-docklike-plugin

    # Sound settings
    pavucontrol
  ];
}
