{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    git # VCS
    tree # Show directories structure as a tree
    wget # Download from link
    fastfetch # System info
    htop # Better top (system monitor)
    killall # Kill all process by name
  ];
}
