{ pkgs, ... }:

let nixPath = "/etc/nixPath";

in
{
  systemd.tmpfiles.rules = [
    "L+ ${nixPath} - - - - ${pkgs.path}"
  ];

  # Configure the nix package manager
  nix = {
    # Set nix path to use nix-* utilities
    # Thanks to https://discourse.nixos.org/t/do-flakes-also-set-the-system-channel/19798/16
    nixPath = [ "nixpkgs=${nixPath}" ];

    # Run garbage collector once a week  
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };

    settings = {
      trusted-users = [ "root" "@wheel" ];
      # Enable flakes
      experimental-features = [ "nix-command" "flakes" ];
    };
  };

  # Enable unfree packages
  nixpkgs.config.allowUnfree = true;
}
