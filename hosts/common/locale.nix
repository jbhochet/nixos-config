{ ... }:

{
  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  console.keyMap = "fr";

  # Set the X11 layout
  services.xserver.xkb.layout = "fr";
}
