{ config, inputs, pkgs, lib, ... }:

let
  username = "jb";
  hostname = config.networking.hostName;
in
{
  imports = [
    inputs.home-manager.nixosModules.home-manager
  ];

  users.users.${username} = {
    isNormalUser = true;

    initialPassword = "changeMe";

    shell = pkgs.zsh;

    # groups for core components
    extraGroups = lib.flatten [
      "wheel" # for sudo
      "networkmanager" # for access network manager config

      (lib.optionals (hostname == "vostro") [
        "vboxusers"
        "wireshark"
      ])
    ];
  };

  # Import home manager config for this user
  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    backupFileExtension = "bak";
    users.${username} = import ../../home-manager/${hostname}.nix;
  };
}
