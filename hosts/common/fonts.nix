{ pkgs, ... }:

{
  # List fonts installed in system profile.
  fonts.packages = with pkgs; [
    dejavu_fonts
    liberation_ttf
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji
    roboto
    # symbola
    (nerdfonts.override { fonts = [ "FiraCode" ]; })
  ];

}
