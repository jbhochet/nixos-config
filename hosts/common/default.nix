{ ... }:

{
  imports =
    [
      ./fonts.nix
      ./locale.nix
      ./network.nix
      ./nix.nix
      ./packages.nix
      ./user.nix
      ./zsh.nix
    ];
}
