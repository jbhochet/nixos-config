{ inputs, pkgs, lib, ... }:

{

  imports =
    [
      inputs.nixos-hardware.nixosModules.common-cpu-intel
      inputs.nixos-hardware.nixosModules.common-gpu-intel
      inputs.nixos-hardware.nixosModules.common-pc-laptop
      inputs.nixos-hardware.nixosModules.common-pc-laptop-ssd

      # Include the results of the hardware scan.
      ./hardware-configuration.nix

      ../common

      ../../nixos/pipewire.nix
      ../../nixos/gnome.nix

      ../../nixos/virtualbox.nix
      ../../nixos/wireshark.nix
      ../../nixos/printer.nix

      ../../nixos/podman.nix
    ];

  boot = {
    # Use the systemd-boot EFI boot loader.
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    # Set the kernel version
    kernelPackages = pkgs.linuxPackages_6_1;

    kernelParams = [ "i915.enable_guc=3" ];

    # Architecture emulation
    binfmt.emulatedSystems = [ "aarch64-linux" ];
  };

  hardware = {
    intelgpu = {
      driver = "i915"; # xe availible with kernel 6.8
      vaapiDriver = "intel-media-driver";
    };
    enableRedistributableFirmware = true;
  };

  # Swap
  swapDevices = lib.mkForce [{
    device = "/var/lib/swapfile";
    size = 8 * 1024;
  }];

  # Improve power management and battery life
  powerManagement.enable = true;

  services = {
    thermald.enable = true;

    power-profiles-daemon.enable = false;

    tlp = {
      enable = true;
      settings = {
        DEVICES_TO_DISABLE_ON_STARTUP = "bluetooth";

        CPU_ENERGY_PERF_POLICY_ON_AC = "balance_power";
        CPU_ENERGY_PERF_POLICY_ON_BAT = "power";

        PLATFORM_PROFILE_ON_AC = "balanced";
        PLATFORM_PROFILE_ON_BAT = "low-power";

        CPU_BOOST_ON_AC = 1;
        CPU_BOOST_ON_BAT = 0;

        CPU_HWP_DYN_BOOST_ON_AC = 1;
        CPU_HWP_DYN_BOOST_ON_BAT = 0;

        RUNTIME_PM_ON_AC = "auto";
        RUNTIME_PM_ON_BAT = "auto";

        WIFI_PWR_ON_AC = "on";
        WIFI_PWR_ON_BAT = "on";
      };
    };

    # Firmware updater
    fwupd.enable = true;

    # Trim SSD disk
    fstrim.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
