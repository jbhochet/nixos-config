{ inputs }: {

  # the unstable nixpkgs set will be accessible through 'pkgs.unstable'
  unstable = (final: prev: {
    unstable = import inputs.nixpkgs-unstable {
      system = prev.system;
    };
  });

}
